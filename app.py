################ Projet Dash - Haris PATEL - Riyâz PATEL - M2 DSS / Dr.Zitouni 
#pip install snscrape
#pip install pandas
#pip install dash
#pip install plotly
#pip install nltk
#pip install wordcloud
#pip install dash_bootstrap_components
######## Imports et donnees préliminaires
#### Imports
import dash
from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from dash import dash_table as dt
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from datetime import date
import plotly.express as px
import pandas as pd
import snscrape.modules.twitter as twitterScraper
from io import BytesIO
from wordcloud import WordCloud
import base64
import time
import nltk
from nltk.corpus import stopwords
import re

#### Imports

#### Stopwords - WC / Fréquence mots
nltk.download('stopwords')

# Pattern pour retirer les caractères indésirables (émoticones, emoji, pictogrammes...) dans le texte des tweets
emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           u"\U00002500-\U00002BEF"  # chinese char
                           u"\U00002702-\U000027B0"
                           u"\U00002702-\U000027B0"
                           u"\U000024C2-\U0001F251"
                           u"\U0001f926-\U0001f937"
                           u"\U00010000-\U0010ffff"
                           u"\u2640-\u2642"
                           u"\u2600-\u2B55"
                           u"\u200d"
                           u"\u23cf"
                           u"\u23e9"
                           u"\u231a"
                           u"\ufe0f"  # dingbats
                           u"\u3030"
                           "]+", flags=re.UNICODE)

# Pattern pour retirer les urls dans le texte des tweets
url_pattern = re.compile(r'(https?://)?(www\.)?(\w+\.)?(\w+)(\.\w+)(/.+)?')

# Stopwords : mots courant qui n'apporte pas de valeurs pour l'analyse textuelle
new_stopwords = stopwords.words('english') + stopwords.words('french') + stopwords.words('arabic') + stopwords.words(
    'german') + stopwords.words('italian') + stopwords.words('spanish')
stopwords_perso = ["c'est", "cest", "quil", "co", "qd", "alors", "si", "tant", "qua", "cela", "tout", "dun", "va",
                   "cette", "cet", "là", "leur", "donc", "ça"]
new_stopwords.extend(stopwords_perso)
#### Stopwords - WC / Frequence mots

#### Valeur des langues la dropdownlist pour le choix des langues
lang_options = [{'label': 'Arabic | العربية', 'value': 'ar'},
                {'label': 'British English | British English', 'value': 'en-gb'},
                {'label': 'English | English', 'value': 'en'}, {'label': 'French | français', 'value': 'fr'},
                {'label': 'German | Deutsch', 'value': 'de'}, {'label': 'Italian | italiano', 'value': 'it'},
                {'label': 'Spanish | español', 'value': 'es'}]
#### Valeur des langues la dropdownlist pour le choix des langues

######## Imports et donnees preliminaires

######## Application Dash

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.MORPH])  # utilisation du theme boostrap :"MORPH"

#### Création du Layout
app.layout = dbc.Container([  # Container permet de centrer et remplir horizontalement le contenu du dashboard
    dbc.Row(dbc.Col(html.H1("Dashboard Twitter Analytics",  # titre du dashboard
                            style={"textAlign": "center"}), width=22)),
    dbc.Row(dbc.Col(html.H1("  Scrapping et analyse de données twitter",  # sous-titre
                            style={"textAlign": "center", "fontSize": 20}), width=22)),
    html.Hr(),  # ajout d'un espace

    html.Div(className='row1',
             style={'display': 'flex'},
             children=[  # regroupe les html.Div() horizontalement dans un children[]
                 html.Div([
                     html.H1('Requête', style={'fontSize': 20}),
                     dcc.Input(id='searchId', placeholder='Ex: vaccin, santé, covid', type='text'),
                     html.Hr(),
                     html.H1('Langue', style={'fontSize': 20}),
                     dcc.Dropdown(id='twitter_search_lang', placeholder='Language',
                                  options=lang_options
                                  )], style={'width': '33%', 'display': 'inline-block'}),
                 # fixer la largeur de html.Div()

                 html.Div([
                     html.H2('Intervalle de temps', style={'fontSize': 20}),
                     dcc.DatePickerSingle(  # création du choix de l'intervalle de temps avec DatepickerSingle
                         id='date1_Id',  # retourne 2 dates en format : 'YYY-MM-DD'
                         min_date_allowed=date(2007, 8, 5),
                         date=date(2022, 1, 1),
                         display_format='YYYY-MM-DD'
                     ),
                     dcc.DatePickerSingle(
                         id='date2_Id',
                         min_date_allowed=date(2007, 8, 5),
                         date=date.today(),
                         display_format='YYYY-MM-DD'
                     )], style={'width': '33%', 'display': 'inline-block'}),

                 html.Div([
                     html.H3('Nombre de tweets', style={'fontSize': 20}),
                     dcc.Input(id='countId',  # Choix du nombre de tweets à scrapper
                               placeholder='Ex: 100, 200', type='number'),
                     dcc.Store(id='memory'), #pour chaque requete, un dataframe est créé et sauvegarde en memoire pour les analyses
                     html.Hr(),
                     html.Hr(),
                     dbc.Button("Lancer", id="button", n_clicks=0, outline=True, color="primary", className="me-1"),
                     # création du bouton "Lancer"
                     dbc.Spinner(html.Div(id="loading-output"), color="primary")],
                     style={'width': '33%', 'display': 'inline-block'})
                 # création de l'animation de chargement avec Spinner()
             ]),

    html.Hr(),
    html.Div([
        dcc.Tabs(
            [  # mise en place de 3 onglets 'Base de données', 'Analyse temporelle' et 'Analyse textuelle' avec Tab()
                dcc.Tab(label='Base de données', children=[
                    dt.DataTable(
                        columns=[{'id': c, 'name': c} for c in
                                 ['URL', 'Text', 'Datetime', 'RetweetCount', 'ReplyCount', 'LikeCount', 'Username']],
                        id='tweet_table',
                        style_cell_conditional=[
                            {
                                'if': {'column_id': c},  # détermine l'apparence des cellules de la table
                                'textAlign': 'left'
                            } for c in ['Date', 'Region']
                        ],
                        style_data={  # détermine l'apparence des données
                            'color': 'black',
                            'backgroundColor': 'white', 'whiteSpace': 'normal', 'height': 'auto'
                        },
                        style_data_conditional=[
                            {
                                'if': {'row_index': 'odd'},
                                'backgroundColor': 'rgb(220, 220, 220)',
                            }
                        ],
                        style_header={  # détermine l'apparence du nom des colonne table
                            'backgroundColor': 'rgb(210, 210, 210)',
                            'color': 'black',
                            'fontWeight': 'bold'
                        },
                        style_table={'overflowY': 'scroll', 'overflowX': 'scroll', 'height': 500}),
                    # permet de scroller horizontalement et verticalement
                    html.Hr(),
                    dbc.Button("Download CSV", id="btn_csv", size="sm", className="me-1", outline=True,
                               color="success"),  # création du bouton de téléchargement
                    dcc.Download(id="download-dataframe-csv")
                ]),
                dcc.Tab(label='Analyse temporelle', children=[
                    dcc.Graph(id='graph-day'),  # graphique du nombre de tweets par jour selon la période choisie
                    dcc.Graph(id='graph-hour')  # graphique du nombre de tweets par heure selon la période choisie
                ]),
                dcc.Tab(label='Analyse textuelle', children=[
                    dcc.Graph(id='tweet_word_count'),  # graphique du nombre d'occurence de chaque mot
                    html.Hr(),
                    dcc.RangeSlider(
                        # création du range slider permettant de choisir le nombre de mot les plus fréquents
                        id='range_frequency_number',
                        min=1,
                        max=200,
                        step=1,
                        value=[1, 200],
                        marks={
                            1: {'label': 'Minimum'},
                            50: {'label': '50'},
                            100: {'label': '100 mots les plus fréquents'},
                            200: {'label': '200'}
                        },
                        pushable=1,
                        tooltip={"placement": "bottom", "always_visible": False},
                        allowCross=False
                    ),
                    html.Hr(),
                    html.Img(id='image_wc', style={  # layout du wordcloud en .Img
                        'height': '100%',
                        'width': '100%'
                    })
                ])
            ])

    ]),
    html.P([html.A("Code source", href="https://gitlab.com/hpatel95/dash-twitter-analytics"),
            # texte en bas de page renvoyant le lien du git
            ".  Projet DASH : Haris PATEL - Riyâz PATEL - M2 DSS / Dr. Zitouni "
            ], style={"textAlign": "center"})
])


#### Création du Layout


#### Creation et stockage du dataframe de tweets scrape
@app.callback(
    [Output("loading-output", "children"),  # sortie de l'animation spinner
     Output('memory', 'data')],  # sortie voulue - data
    [Input("button", "n_clicks")],  # bouton pour lancer la commande
    [State('searchId', 'value'),  # bouton pour recuperer la valeur entree par l'utilisateur
     State('twitter_search_lang', 'value'),  # dropdown pour choix de la langue
     State('date1_Id', 'date'),  # premiere valeur de l'intervalle
     State('date2_Id', 'date'),  # deuxieme valeur de l'intervalle
     State('countId', 'value')]  # nombre de tweet max a recuperer

)
def update_Frame(n_clicks, searchId, twitter_search_lang, date_since, date_until, count):
    if searchId is None:
        raise PreventUpdate  # empecher le lancement s'il n'y a rien en entree
    # fonction pour scrapper les tweets
    if n_clicks:
        time.sleep(1)
        tweet_list = []  # on creer une lsite vide pour accueillir les tweets
        for i, tweet in enumerate(twitterScraper.TwitterSearchScraper(
                query=searchId + " since:" + date_since + " until:" + date_until + " lang:" + twitter_search_lang).get_items()):  # "since:" et "until:" sont les commandes qui vont permettre de faire des recherches selon des dates (attention la date until est n-1), "lang:" permet de lancer une requete dans une des langues choisi de la liste
            if i > count:  # quand on atteint le nombre de tweets souhaité on arrete de chercher
                break
            tweet_list.append(
                [tweet.url, tweet.content, tweet.date, tweet.replyCount, tweet.retweetCount, tweet.likeCount,
                 tweet.user.username])  # choix des valeurs a garder pour creer le df
        # Creer un dataframe a partir de la liste de tweets
        df = pd.DataFrame(tweet_list,
                          columns=['URL', 'Text', 'Datetime', 'RetweetCount', 'ReplyCount', 'LikeCount',
                                   'Username'])
        return f"Données chargées", df.to_dict(
            orient='records')  # on retourne le df en .to_dict, afin de pouvoir le sauvegarder (dcc.store) et le reutiliser pour les analyses


#### Creation et stockage du dataframe de tweets scrape


#### Affichage de la base de donnee
@app.callback(
    [
        Output(component_id='tweet_table', component_property='data'),
        # on exporte les donnees et les colonnes pour afficher la bdd avec l'element dt.DataTable
        Output(component_id='tweet_table', component_property='columns')],
    [Input(component_id='memory', component_property='data')]
)
def display_tweets(df):
    tweets = pd.DataFrame(
        df)  # on repasse le df stocke en .df pour recuperer d'abordles valeurs des colonnes pour l'affichage
    columns = [{'name': col, 'id': col} for col in tweets.columns]
    data = tweets.to_dict(orient='records')
    return data, columns


#### Affichage de la base de donnee


#### Graph : Nombre de tweets par jour
@app.callback(
    Output(component_id='graph-day', component_property='figure'),
    [Input('memory', 'data')],
    [State('date1_Id', 'date'),
     # on utilise les intervalles pour pouvoir remplir les dates manquantes par la valeur 0 (dans le cas ou aucun tweet n'existe pour un jour)
     State('date2_Id', 'date')]
)
def update_div1(df, min_date_range, max_date_range):
    if df is None:
        raise PreventUpdate  # empecher le lancement s'il n'y a rien en entree

    data = pd.DataFrame(
        df)  # on recupere la base initiale stocké (memory) et on l'utilise pour le graph en le passant en .DataFrame

    # Compte des tweets/jour
    data['date'] = pd.DatetimeIndex(data['Datetime']).date
    data['count'] = 1
    data_filtered = data[['date', 'count']]

    df_tweets_date = data_filtered.groupby(["date"]).sum().reset_index()  # passe l'index en colonne

    df_tweets_date.set_index('date',
                             inplace=True)  # on passe les dates en index pour pouvoir ensuite rajouter les manquants

    df_tweets_date = df_tweets_date.reindex(pd.date_range(min_date_range, max_date_range),
                                            fill_value=0)  # on utilise les intervalles pour pouvoir remplir les dates manquantes par la valeur 0 (dans le cas ou aucun tweet n'existe pour un jour)

    # on plot le graph
    fig = px.line(df_tweets_date, x=df_tweets_date.index, y='count',
                  title='Nombre de tweets quotidien')

    fig.update_layout(xaxis=dict(title='Date'),
                      yaxis=dict(title='Nombre de tweets'), title_x=.5,
                      margin=dict(r=0))

    return fig


#### Graph : Nombre de tweets par jour


#### Graph : Repartition des tweets en 24heures
@app.callback(
    Output(component_id='graph-hour', component_property='figure'),
    [Input('memory', 'data')]
)
def update_div2(df):
    if df is None:
        raise PreventUpdate  # empecher le lancement s'il n'y a rien en entree

    data = pd.DataFrame(df)
    data['hour'] = pd.DatetimeIndex(data['Datetime']).hour  # on recupere les heures uniquement

    data['count'] = 1

    data = data[['hour', 'count']]

    data['hour'] = pd.to_datetime(data['hour'], format='%H')  # on transforme au format heure pour la suite

    # creation du df avec le nombre de tweets selon l'heure de la journee
    df_tweets_hourly = data.groupby(
        ["hour"]).sum().reset_index()  # comme precedement on passe la date en index pour le calcul

    df_tweets_hourly.set_index('hour', inplace=True)

    df_tweets_hourly = df_tweets_hourly.reindex(
        pd.date_range(start="1900-01-01 00:00:00", end="1900-01-01 23:00:00", freq="60min"),
        fill_value=0)  # pour les heures qui seriant manquantes, on assigne la valeur 0 au compte

    df_tweets_hourly[
        'datetime'] = df_tweets_hourly.index  # une fois le compte et les ajouts termines, on repasse la date en colonne
    df_tweets_hourly['hour'] = pd.DatetimeIndex(
        df_tweets_hourly['datetime']).hour  # on recupere toutes les heures, comptabilises

    fig = px.bar(df_tweets_hourly, x='hour', y='count', range_x=[0, 23],
                 title='Répartition des tweets sur 24 heures')

    fig.update_layout(xaxis=dict(title='Heure de la journée'),
                      yaxis=dict(title='Nombre de tweets'), title_x=.5,
                      margin=dict(r=0))

    return fig


#### Graph : Repartition des tweets en 24heures

#### Bouton pour telecharger la base de donnees des tweets scrapes, memory, en csv
@app.callback(
    Output('download-dataframe-csv', 'data'),
    [Input('btn_csv', 'n_clicks')],  # bouton pour lancer le telechargement
    [State('memory', 'data')],
    prevent_initial_call=True,  # empecher le lancement automatique
)
def func(n_clicks, df):
    data = pd.DataFrame(df)  # on passe la bdd stocke dans dcc.store en pd.DataFrame
    return dcc.send_data_frame(data.to_csv, "tweets_scrapes.csv")  # on exporte le DataFrame en csv


#### Bouton pour telecharger la base de donnees des tweets scrapes, memory, en csv

#### Graph : Frequence des mots des tweets et WordCloud
@app.callback(
    [Output(component_id='tweet_word_count', component_property='figure'),  # graph frequence
     Output('image_wc', 'src')],  # image wordcloud
    [Input('memory', 'data'),
     Input('range_frequency_number', 'value')  # slider pour choisir les x nombre de mots different a compter
     ]
)
def word_count_graph(df, count_value):
    if df is None:
        raise PreventUpdate  # empecher le lancement s'il n'y a rien en entree

    data = pd.DataFrame(df)

    # convertir le contenu du tweet en minuscule
    data['clean_text'] = data['Text'].apply(
        lambda x: ' '.join([word for word in x.lower().split() if word not in (new_stopwords)]))
    # enlever les emojis
    data['clean_text'] = data['clean_text'].str.replace(emoji_pattern, '', regex=True)
    # enlever les liens
    data['clean_text'] = data['clean_text'].str.replace(url_pattern, '', regex=True)
    # Remplacer la ponctuation par un espace
    data['clean_text'] = data['clean_text'].str.replace(r'[^\w\s]+', ' ', regex=True)
    # Filtrer de nouveau avec les stopwords
    data['clean_text'] = data['clean_text'].apply(
        lambda x: ' '.join([word for word in x.split() if word not in (new_stopwords)]))
    # Compter le nombre de chaque mot
    countFreq = data.clean_text.str.split(expand=True).stack().value_counts()[:100]
    # Ajuster le nombre de mots selon le choix de l'utilisateur
    newGraph = countFreq[count_value[0]:count_value[1]]

    # on plot le graph
    fig = px.bar(x=newGraph.index, y=newGraph.values, title="Fréquence de mots des tweets")

    fig.update_layout(xaxis=dict(title='Mots'),
                      yaxis=dict(title='Compte'), title_x=.5,
                      margin=dict(r=0))

    # on cree une image pour le wordcloud
    wc = WordCloud(background_color='#F0FFFE', width=800, height=600).generate_from_frequencies(newGraph)
    wc_img = wc.to_image()
    img = BytesIO()
    wc_img.save(img, format='PNG')

    return fig, "data:image/png;base64,{}".format(
        base64.b64encode(img.getvalue()).decode())  # on retourne le graph plotly et l'image


#### Graph : Frequence des mots des tweets et WordCloud

######## Application Dash


if __name__ == '__main__':
    app.run_server(debug=True)
